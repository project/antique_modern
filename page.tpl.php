<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
  <!-- Begin page header -->
  <div id="head">
    <div id="site-name"><?php print $site_name; ?></div>
    <?php if ($site_slogan) { ?>
      <div class='site-slogan'><?php print $site_slogan ?></div>
    <?php } ?>
    <?php if (isset($primary_links)) { ?>
      <div id="menu">
      <?php print theme('menu_links', $primary_links); ?>
      </div>
    <?php } ?>
  </div>
<!-- End page header -->

    <div id="body_wrapper">
      <div id="body">
        <!-- begin subheader -->
        <div id="subhead">
          <?php if ($search_box): ?>
            <div class="search_box"><?php print $search_box ?></div>
          <?php endif; ?>
          <?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('links', $secondary_links) ?></div><?php } ?>
          <?php if ($header): ?>
            <div><?php print $header ?></div>
          <?php endif; ?>
        </div>
        <!-- end subheader -->
        <!-- begin main content -->
        <?php if ($sidebar_left || $sidebar_right): ?>
          <div id="left">
        <?php else: ?>
          <div id="all">
        <?php endif; ?>
          <div class="top"></div>
          <div class="content">
            <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
            <div id="main">
              <?php if ($breadcrumb || $title): ?>
                <div class="breadcrumb"> <?php print $breadcrumb ?>
                <?php if ($title && $breadcrumb) print " &raquo; "; ?>
                <span class="title"><?php print $title ?></span>
                </div>
              <?php endif; ?>
              <div class="tabs"><?php print $tabs ?></div>
              <div class="clearer">&nbsp;</div>
              <?php print $help ?>
              <?php print $messages ?>
              <?php print $content; ?>
            </div>
          </div>
          <div class="bottom"></div>
        </div>
        <?php if ($sidebar_left || $sidebar_right): ?>
          <div id="right">
            <div class="top"></div>
            <?php if ($sidebar_left) {  print $sidebar_left; } ?>
            <?php if ($sidebar_right) { print $sidebar_right; } ?>
            <div class="bottom"></div>
          </div>
        <?php endif; ?>
          <div class="clearer"></div>
      </div>
      <div class="clearer"></div>
    </div>
    <div id="end_body"></div>

    <div id="footer">
      <?php print $footer_message ?>
    </div>
<?php print $closure ?>
</body>
</html>
