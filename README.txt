This theme is a port of Antique Modern by wildleaf (http://aaron.ganschow.us/) 
from oswd.org.

The original theme can be found here: 

http://www.oswd.org/design/preview/id/2782

This theme is fixed width; it supports only the right sidebar or no sidebar
(it has slightly different modes if there is no sidebar). As a convenience
the contents of the left sidebar are applied to the right.

It's a little funky in Opera for reasons I can't figure out, annoyingly
enough.

It does not support a logo; if you want a logo with this theme you'll have
to try to put one in manually.

The original theme didn't really have a concent of Drupal's nice menu
structure, so the theme has been changed somewhat to compensate for that.
As near as I can tell it supports most of the Drupal core modules, though
I didn't test every single thing.

I will probably not do too much maintenance of this theme once I'm done
with it, so hopefully everything will just work for you.


