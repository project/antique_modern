<?php
/**
 * Not really a theme function, but it lets us do our menu with nice tabs,
 * and keeps the active item active.
 */
function phptemplate_menu_links($items, $type = 'ul') {
  if (!empty($items)) {
    $output .= "<$type>";
    foreach ($items as $link) {
      $active = '';
      if ($_GET['q'] == $link['href']) {
        $active = ' class="active"';
      }
      $output .= "<li$active>\n";
      // Is the title HTML?
      $html = isset($link['html']) && $link['html'];

      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
      }
      $output .= "</li>\n";
    }
    $output .= "</$type>";
  }

  return $output;
}

/**
 * Move the <div> for the breadcrumb out so we can put our title with it.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return implode(' &raquo; ', $breadcrumb);
  }
}

function phptemplate_blocks($region) {
  $output = '';

  if ($list = module_invoke('block', 'list', $region)) {
    foreach ($list as $key => $block) {
      // $key == <i>module</i>_<i>delta</i>
      if ($output) {
        $output .= '<hr />';
      }
      $output .= theme('block', $block);
    }
  }

  // Add any content assigned to this region through drupal_set_content() calls.
  $output .= drupal_get_content($region);

  return $output;
}

/**
 * Custom pager
 */

function phptemplate_pager($tags = array(), $pageSize = 10, $element = 0, $attributes = array()) {
  global $pager_page_array, $pager_total, $pager_total_items;
  $output = "";

  $pageNum = $pager_page_array[$element] + 1;

  $numPages = $pager_total[$element];

  if ($pager_total_items[$element] > $pageSize) {
    $output .= '<div class="pager-top">';
    $output .= _pagerPrev($pageSize, $element, $attributes, $pageNum, $numPages);
    $output .= _pagerNext($pageSize, $element, $attributes, $pageNum, $numPages);
    $output .= _pagerPageNum($pageSize, $element, $attributes, $pageNum, $numPages);
    $output .= '</div>';

  }
  return $output;
}

function _pagerUrl($pageNum, $text, $element, $attributes, $pageSize) {
  global $pager_page_array;
//  $from = ($pageNum - 1) * $pageSize;

  $from_new = pager_load_array($pageNum - 1, $element, $pager_page_array);
  
  return theme('pager_link', $text, $from_new, $element, $attributes);
}

function _pagerPageNum($pageSize, $element, $attributes, $pageNum, $numPages) {
  return "<div class='page-middle'>" . _pagerLittlePrevButton($pageSize, $element, $attributes, $pageNum, $numPages) 
    . "<span class='page-num'>Page $pageNum</span>" 
    . _pagerLittleNextButton($pageSize, $element, $attributes, $pageNum, $numPages)
    . "</div>\n";

}

function _pagerLittlePrevButton($pageSize, $element, $attributes, $pageNum, $numPages)
{
	// Is there a previous button to even print?
	// page < 1 == page 1.
	if ($pageNum <= 1)
		return "";

  $prev = $pageNum - 1;
	return _pagerUrl($prev, "<<", $element, $attributes, $pageSize);
}

function _pagerLittleNextButton($pageSize, $element, $attributes, $pageNum, $numPages)
{
	// Is there a previous button to even print?
	// page < 1 == page 1.
	if ($pageNum >= $numPages)
		return "";

  $next = $pageNum + 1;
	return _pagerUrl($next, ">>", $element, $attributes, $pageSize);
}

// function _pagerPrevButton($url, $page, $pageSize, $defaultPageSize, $count)
function _pagerPrev($pageSize, $element, $attributes, $page, $numPages)
{
	// Is there a previous button to even print?
	// page < 1 == page 1.
	if ($page <= 1)
		return "";

	// First, let's just do the previous page.
	$prev = $page - 1;
	$string = _pagerUrl($prev, $prev, $element, $attributes, $pageSize);

	// Let's do five pages, plus page #1, and a ... if there's a blank spot.
	for ($i = $page - 2; $i > max(1, ($page - 5)); $i--)
		$string = _pagerUrl($i, $i, $element, $attributes, $pageSize) . " $string";

	if ($i != 1 && $i != 0) // if we stopped at something other than 1 there was > 5
		$string = "... " . $string;

  // Now see if we need to do powers of 10.
	for ($counter = 1; $counter < 5; $counter++) {
		$pow = pow(10, $counter);
		if ($page > 16 * pow(10, $counter - 1))
			for ($i = floor((($page)/$pow) - 1) * $pow, $j = 0; $i > 1 && $j < 2; $i -= $pow, $j++)
				$string = _pagerUrl($i, $i, $element, $attributes, $pageSize) . " $string";
	}

  // And finally, the very first page always shows up, unless we've already hit it.
	if ($prev != 1)
		$string = _pagerUrl(1, 1, $element, $attributes, $pageSize) . " $string";

	return "<div class='pager-prev'>$string</div>";
}

function _pagerNext($pageSize, $element, $attributes, $page, $numPages)
{
	// Is there a next button to even print?
	if ($page >= $numPages)
		return "";

	// First, let's just do the previous page.
	$next = $page + 1;
	$string = _pagerUrl($next, $next, $element, $attributes, $pageSize);
	// Let's do five pages, plus page #1, and a ... if there's a blank spot.

	for ($i = $page + 2; $i < min($numPages, ($page + 5)); $i++)
		$string .= " " . _pagerUrl($i, $i, $element, $attributes, $pageSize);
	if ($i != $numPages && $i != $numPages + 1) // if we stopped at something other than 1 there was > 5
		$string .= " ...";

	for ($counter = 1; $counter < 5; $counter++) {
		$pow = pow(10, $counter);
		if (($numPages - $page) > 11 * pow(10, $counter - 1))
			for ($i = ceil(($page + 5)/$pow) * $pow, $j = 0; $i < $numPages && $j < 2; $i += $pow, $j++)
				$string .= " " . _pagerUrl($i, $i, $element, $attributes, $pageSize);
	}
	if ($next != $numPages)
		$string .= " " . _pagerUrl($numPages, $numPages, $element, $attributes, $pageSize);
	return "<div class='pager-next'>$string</div>";
 
}

function theme_user_login_block($form) {
  if (variable_get('user_register', 1)) {
    $items[] = l(t('Register'), 'user/register', array('title' => t('Create a new user account.')));
  }
  $items[] = l(t('Forgot password?'), 'user/password', array('title' => t('Request new password via e-mail.')));
  $form['links']['#value'] = theme('item_list', $items);

  $output .= form_render($form);
  return $output;
}
